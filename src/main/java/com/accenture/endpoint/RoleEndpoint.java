package com.accenture.endpoint;

import com.accenture.service.RoleService;
import com.accenture.soap_service_api_exercise.GetRoleRequest;
import com.accenture.soap_service_api_exercise.GetRoleResponse;
import com.accenture.soap_service_api_exercise.RoleInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class RoleEndpoint {

    private static final String NAMESPACE_URI = "http://accenture.com/soap-service-api-exercise";

    @Autowired
    private RoleService roleService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRoleRequest")
    @ResponsePayload
    public GetRoleResponse getRole(@RequestPayload GetRoleRequest request){
        GetRoleResponse response = new GetRoleResponse();
        RoleInfo role = new RoleInfo();
        BeanUtils.copyProperties(roleService.getRoleName(request.getRoleName()),role);
//        BeanUtils.copyProperties(roleService.getRolesById(request.getRoleId()),role);
        response.setRoleInfo(role);

        return response;
    }

}
