package com.accenture.repository;

import com.accenture.entities.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Long> {
    Roles findByRoleName(String roleName);
//    Roles findByRoleId(long id);

//   Roles findByName(String role);
}
