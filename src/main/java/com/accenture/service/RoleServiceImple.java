package com.accenture.service;

import com.accenture.entities.Roles;
import com.accenture.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImple implements RoleService {

    @Autowired
    private RoleRepository roleRepository;


//        @Override
//    public Roles getRolesById(long id) {
//            Roles roles = roleRepository.findByRoleId(id);
//        return roles;
//    }


    @Override
    public Roles getRoleName(String roleName) {
        return roleRepository.findByRoleName(roleName);
    }
}
