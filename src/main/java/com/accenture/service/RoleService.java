package com.accenture.service;

import com.accenture.entities.Roles;

import java.util.List;

public interface RoleService {
//    Roles getRolesById(long id);
    Roles getRoleName(String roleName);
}
