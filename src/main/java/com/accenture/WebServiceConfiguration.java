package com.accenture;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfiguration extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext){
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet,"/roleSchema/*");
    }

    @Bean
    public XsdSchema roleSchema(){
        return new SimpleXsdSchema(new ClassPathResource("roles.xsd"));
    }

    @Bean(name = "role")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema roleSchema){
        DefaultWsdl11Definition defaultWsdl11Definition = new DefaultWsdl11Definition();
        defaultWsdl11Definition.setPortTypeName("RoleService");
        defaultWsdl11Definition.setLocationUri("/roleSchema");
        defaultWsdl11Definition.setTargetNamespace("http://accenture.com/soap-service-api-exercise");
        defaultWsdl11Definition.setSchema(roleSchema);
        return defaultWsdl11Definition;
    }
}
